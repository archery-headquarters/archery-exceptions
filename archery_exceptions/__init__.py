class PasswordError(Exception):
    def __init__(
        self: object,
        cause: str
    ) -> None:
        self.cause = cause

    def __str__(self: object) -> str:
        return f'Wrong password, { self.cause }'


class EnvVarError(Exception):
    def __init__(
        self: object,
        variable: str
    ) -> None:
        self.variable = variable

    def __str__(self: object) -> str:
        return f'Required variable "{ self.variable }" not found.'


class DataBaseProviderError(Exception):
    def __init__(
        self: object,
        value: str
    ) -> None:
        self.value = value

    def __str__(self: object) -> str:
        return f'Database provider "{ self.value }" not available.'


class NullValueError(Exception):
    def __init__(
        self: object,
        value: str
    ) -> None:
        self.value = value

    def __str__(self: object) -> str:
        return f'Argument "{ self.value }" cannot be null.'
