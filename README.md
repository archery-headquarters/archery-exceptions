# Archery Exceptions
Exceptions for the Archery ecosystem.

The following exceptions are found in this package:

* PasswordError

Used in case of weak or bad password.
* EnvVarError

Used when an environment variable necessary for the system to function is missing.
* DataBaseProviderError

Used when it is not possible to connect to the database due to an error in the provider name.
* NullValueError

This exception is raised when a required argument is not passed.

### How to use
```python
from os import getenv
from sqlite3 import connect
from archery_exceptions import (
    PasswordError, EnvVarError, DataBaseProviderError, NullValueError
)


def find_error(password, db_file, required_value=None):
    if not getenv('NULL_VARIABLE'):
        raise EnvVarError('NULL_VARIABLE')
    if password < 6:
        raise PasswordError('Password too short.')
    try:
        connect(db_file)
    except Exception:
        raise DataBaseProviderError(db_file)
    if required_value == None:
        NullValueError('required_value')
```
